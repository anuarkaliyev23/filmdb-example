package org.bitbucket.anuarkaliyev23.home.cinema.db.parser;

import org.bitbucket.anuarkaliyev23.home.cinema.db.command.*;
import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Film;
import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Genre;
import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Person;
import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Role;
import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.FilmStorage;

import java.util.Scanner;

// film add Amazing Spider Man, 2017, Mark, Webb, Drama
// film list
// film find-title Amazing Spider Man
// film find-director Mark, Webb
// film delete Amazing Spider Man, 2017
public class CLIParser implements Parser {
    private FilmStorage storage;

    public FilmStorage getStorage() {
        return storage;
    }

    public void setStorage(FilmStorage storage) {
        this.storage = storage;
    }

    public CLIParser(FilmStorage storage) {
        this.storage = storage;
    }

    private Command parseFilmAddCommand(Scanner scanner) {
        String line = scanner.nextLine();
        String[] tokens = line.split("[,]");

        String title = tokens[0].trim();
        int year = Integer.parseInt(tokens[1].trim());

        String directorFirstName = tokens[2].trim();
        String directorLastName = tokens[3].trim();
        Person director = new Person(directorFirstName, directorLastName, Role.DIRECTOR);

        Genre genre = Genre.valueOf(tokens[4].trim().toUpperCase());

        Film f = new Film(title, year, director, genre);
        return new FilmAddCommand(storage, f);
    }

    private Command parseFilmListCommand() {
        return new FilmListCommand(storage);
    }

    private Command parseFilmDeleteCommand(Scanner scanner) {
        String line = scanner.nextLine();
        String[] tokens = line.split("[,]");

        String title = tokens[0].trim();
        int year = Integer.parseInt(tokens[1].trim());
        return new FilmDeleteCommand(storage, title, year);
    }

    private Command parseFilmFindByDirectorCommand(Scanner scanner) {
        String line = scanner.nextLine();
        String[] tokens = line.split("[,]");

        String firstName = tokens[0].trim();
        String lastName = tokens[1].trim();
        Person director = new Person(firstName, lastName, Role.DIRECTOR);
        return new FilmFindByDirectorCommand(storage, director);
    }

    private Command parseFilmFindByTitleCommand(Scanner scanner) {
        String line = scanner.nextLine();
        return new FilmFindByTitleCommand(storage, line.trim());
    }

    @Override
    public Command parse(String line) {
        Scanner scanner = new Scanner(line);
        String firstToken = scanner.next();
        if (!firstToken.equals("film")) {
            throw new RuntimeException("Not a film command!");
        }

        String commandName = scanner.next();

        switch (commandName) {
            case "add" : return parseFilmAddCommand(scanner);
            case "list" : return parseFilmListCommand();
            case "find-title": return parseFilmFindByTitleCommand(scanner);
            case "find-director": return parseFilmFindByDirectorCommand(scanner);
            case "delete": return parseFilmDeleteCommand(scanner);
            default: throw new RuntimeException("Not supported Command");
        }
    }
}
