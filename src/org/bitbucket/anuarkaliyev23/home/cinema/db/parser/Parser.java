package org.bitbucket.anuarkaliyev23.home.cinema.db.parser;

import org.bitbucket.anuarkaliyev23.home.cinema.db.command.Command;

public interface Parser {
    Command parse(String line);
}
