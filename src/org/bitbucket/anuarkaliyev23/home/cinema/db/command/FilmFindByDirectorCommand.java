package org.bitbucket.anuarkaliyev23.home.cinema.db.command;

import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Film;
import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Person;
import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.FilmStorage;

import java.util.List;

public class FilmFindByDirectorCommand extends FilmCommand{
    private final Person director;

    public FilmFindByDirectorCommand(FilmStorage storage, Person director) {
        super(storage);
        this.director = director;
    }

    public Person getDirector() {
        return director;
    }

    @Override
    public void execute() {
        List<Film> films = getStorage().findFilmByDirector(director);
        for (Film f : films) {
            System.out.println(f);
        }
    }
}
