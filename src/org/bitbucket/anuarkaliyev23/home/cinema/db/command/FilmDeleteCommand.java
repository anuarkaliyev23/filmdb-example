package org.bitbucket.anuarkaliyev23.home.cinema.db.command;

import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.FilmStorage;

public class FilmDeleteCommand extends FilmCommand{
    private final String title;
    private final int year;

    public FilmDeleteCommand(FilmStorage storage, String title, int year) {
        super(storage);
        this.title = title;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    @Override
    public void execute() {
        getStorage().deleteFilm(title, year);
    }
}
