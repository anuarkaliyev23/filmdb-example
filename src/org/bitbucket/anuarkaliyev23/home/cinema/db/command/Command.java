package org.bitbucket.anuarkaliyev23.home.cinema.db.command;

public interface Command {
    void execute();
}
