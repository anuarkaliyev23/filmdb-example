package org.bitbucket.anuarkaliyev23.home.cinema.db.command;

import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Film;
import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.FilmStorage;

import java.util.List;

public class FilmFindByTitleCommand extends FilmCommand {
    private final String title;

    public FilmFindByTitleCommand(FilmStorage storage, String title) {
        super(storage);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public void execute() {
        List<Film> films = getStorage().findFilmByTitle(title);
        for (Film f: films) {
            System.out.println(f);
        }
    }
}
