package org.bitbucket.anuarkaliyev23.home.cinema.db.command;

import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Film;
import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.FilmStorage;

public class FilmAddCommand extends FilmCommand {
    private final Film film;

    public FilmAddCommand(FilmStorage storage, Film film) {
        super(storage);
        this.film = film;
    }


    public Film getFilm() {
        return film;
    }

    @Override
    public void execute() {
        getStorage().addFilm(film);
    }
}
