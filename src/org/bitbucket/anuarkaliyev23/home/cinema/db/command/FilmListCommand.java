package org.bitbucket.anuarkaliyev23.home.cinema.db.command;

import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Film;
import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.FilmStorage;

public class FilmListCommand extends FilmCommand {
    public FilmListCommand(FilmStorage storage) {
        super(storage);
    }

    @Override
    public void execute() {
        for (Film f : getStorage().allFilms()) {
            System.out.println(f);
        }
    }
}
