package org.bitbucket.anuarkaliyev23.home.cinema.db.command;

import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.FilmStorage;

public abstract class FilmCommand implements Command {
    private final FilmStorage storage;

    public FilmCommand(FilmStorage storage) {
        this.storage = storage;
    }

    public FilmStorage getStorage() {
        return storage;
    }
}
