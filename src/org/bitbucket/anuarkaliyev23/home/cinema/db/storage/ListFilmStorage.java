package org.bitbucket.anuarkaliyev23.home.cinema.db.storage;

import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Film;
import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Person;

import java.util.ArrayList;
import java.util.List;

public class ListFilmStorage implements FilmStorage{
    private List<Film> films;

    public ListFilmStorage() {
        films = new ArrayList<>();
    }

    public ListFilmStorage(List<Film> films) {
        this.films = films;
    }

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }

    @Override
    public List<Film> allFilms() {
        return films;
    }

    @Override
    public List<Film> findFilmByTitle(String title) {
        List<Film> result = new ArrayList<>();

//        for (int i = 0; i < films.size(); i++) {
//            Film film = films.get(i);
//            if (film.getTitle().equals(title)) {
//                result.add(film);
//            }
//        }

        for (Film film : films) {
            if (film.getTitle().equals(title))
                result.add(film);
        }

//        films.forEach(film -> {
//            if (film.getTitle().equals(title)) {
//                result.add(film);
//            }
//        });

        return result;
    }

    @Override
    public List<Film> findFilmByDirector(Person director) {
        List<Film> result = new ArrayList<>();

        for (Film f : films) {
            if (f.getDirector().equals(director)) {
                result.add(f);
            }
        }

        return result;
    }

    @Override
    public void deleteFilm(String title, int year) {
        Film filmToDelete = null;

        for (Film film : films) {
            if (film.getTitle().equals(title) && film.getPublicationYear() == year) {
                filmToDelete = film;
            }
        }

        if (filmToDelete != null) {
            films.remove(filmToDelete);
        }
    }

    @Override
    public void addFilm(Film film) {
        films.add(film);
    }
}
