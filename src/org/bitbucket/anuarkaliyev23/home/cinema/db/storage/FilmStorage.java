package org.bitbucket.anuarkaliyev23.home.cinema.db.storage;

import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Film;
import org.bitbucket.anuarkaliyev23.home.cinema.db.model.Person;

import java.util.List;

public interface FilmStorage {
    List<Film> allFilms();
    List<Film> findFilmByTitle(String title);
    List<Film> findFilmByDirector(Person director);
    void deleteFilm(String title, int year);
    void addFilm(Film film);
}
