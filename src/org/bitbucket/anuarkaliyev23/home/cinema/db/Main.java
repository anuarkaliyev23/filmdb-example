package org.bitbucket.anuarkaliyev23.home.cinema.db;

import org.bitbucket.anuarkaliyev23.home.cinema.db.command.Command;
import org.bitbucket.anuarkaliyev23.home.cinema.db.parser.CLIParser;
import org.bitbucket.anuarkaliyev23.home.cinema.db.parser.Parser;
import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.FilmStorage;
import org.bitbucket.anuarkaliyev23.home.cinema.db.storage.ListFilmStorage;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FilmStorage storage = new ListFilmStorage();
        Parser parser = new CLIParser(storage);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print(">");
            String line = scanner.nextLine();
            Command command = parser.parse(line);
            command.execute();
        }
    }
}
