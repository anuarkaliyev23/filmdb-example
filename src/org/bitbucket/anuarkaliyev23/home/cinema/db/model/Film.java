package org.bitbucket.anuarkaliyev23.home.cinema.db.model;

import java.util.Objects;

public class Film {
    private String title;
    private int publicationYear;
    private Person director;
    private Genre genre;

    public Film(String title, int publicationYear, Person director, Genre genre) {
        this.title = title;
        this.publicationYear = publicationYear;
        this.director = director;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Film film = (Film) o;
        return publicationYear == film.publicationYear &&
                Objects.equals(title, film.title) &&
                Objects.equals(director, film.director) &&
                genre == film.genre;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, publicationYear, director, genre);
    }

    @Override
    public String toString() {
        return "Film{" +
                "title='" + title + '\'' +
                ", publicationYear=" + publicationYear +
                ", director=" + director +
                ", genre=" + genre +
                '}';
    }
}
