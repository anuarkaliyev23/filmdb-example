package org.bitbucket.anuarkaliyev23.home.cinema.db.model;

public enum Genre {
    COMEDY,
    HORROR,
    THRILLER,
    DETECTIVE,
    DRAMA,
    HISTORY;
}
