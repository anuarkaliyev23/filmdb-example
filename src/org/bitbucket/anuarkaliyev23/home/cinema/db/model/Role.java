package org.bitbucket.anuarkaliyev23.home.cinema.db.model;

public enum Role {
    DIRECTOR,
    ACTOR;
}
